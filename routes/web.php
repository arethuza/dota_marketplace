<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template', ['kategori' => []]);
});

Route::get('/aku', function(){
	return "Halo aku!";
});

Route::get('/halo/{namaq}', function($namaq){
	return "Halo $namaq";
});

Route::get('/halo/{namaq}/{nama}', function($namaq, $asmane){
	return "Halo, $namaq $asmane !";
});

Route::get('/users', 'ControllerKu@ads');

Route::get('/adds/{nana}', 'ControllerKu@add');

Route::post('/kategori', 'KategoriController@submit');

Route::post('/kategori/delete', 'KategoriController@delete');

Route::post('/edit', 'KategoriController@edit');