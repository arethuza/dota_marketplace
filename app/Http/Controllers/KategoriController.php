<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class KategoriController extends Controller {

	public function index()
	{
		
	}

	public function submit(Request $request)
	{
		if (Session::has('kategori')) {
			Session::push('kategori', $request->kategori);
		} else {
			Session::put('kategori', array($request->kategori));
		}

		return redirect('/kategori');
	}

	public function delete($id) 
	{
		$kategori = Session::get('kategori');
		if (array_key_exists($id, $kategori)) {
			unset($kategori[$id]);
			Session::put('kategori', $kategori);
		}

		return redirect('/kategori');
	}


	public function edit(Request $request)
	{
		$kategori = Session::get('kategori');
		if (array_key_exists($request->ket,$kategori))
		{
			$kategoriUpdate = array_replace($kategori, array($request->key=>$request->kategori));
			Session::put('kategori', $kategoriUpdate);
		}

		return redirect('/kategori');
	}

}

/* End of file KategoriController.php */
/* Location: .//E/xampp/htdocs/dota_cademy/dota_marketplace/app/Http/Controllers/KategoriController.php */