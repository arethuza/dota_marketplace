
@include('base.header')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contoh Form dan Tabel
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Form</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="{{url('/kategori')}}" method="post">
						
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Kategori</label>
								<input type="text" name="kategori" class="form-control" placeholder="Masukkan nama kategori produk">
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Contoh Tabel</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Nama Kategori</th>
							</tr>
							@if(session('kategori'))
							@foreach(session('kategori') as $key=>$value)
							<tr>
								<td>{{ $key }}</td>
								<td>{{ $value }}</td>
								<td>
									<button class="btn btn-primary" onclick="edit('{{$value}}', '{{$key}}')" data-toggle="modal" data-target="#modal-edit"><span class="fa fa-edit"></span></button>
									<a href="{{url('kategori/delete/'.$key)}}" class="btn btn-danger"><span class="fa fa-trash"></span></a>
								</td>
							</tr>
							@endforeach
							@endif
						</table>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

@include('base.footer')